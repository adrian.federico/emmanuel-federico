#!/bin/bash

#TAREA 04

#SCRIPT DE IPTABLES

#Carrera: Tecnicatura en Soporte de Infraestructura
#Profesor: Sergio Pernas
#Alumno: Adrian Emmanuel Federico


#Validar usuario 'root'.

if [ ! $UID -eq 0 ]; then 
	echo "Ejecute este programa como 'root'."
        exit 1 
fi

#Definir variables
BACKUP=/var/backups/reglas.bak
IPTABLES="Aplicar reglas de Firewall. Ejecute:\n

Acciones:\n
\t\tlist\tListar reglas\n
\t\tres\tRestablecer reglas por defecto\n
\t\tback\tCrear back-up reglas\n
\t\tdel\tEliminar regla\n
\t\tmove\tMover regla\n"


case $1 in
list)
	echo "Politicas actualmente establecidas:" && iptables -L -n --line-numbers
        sleep 1s
        read -e -p "Desea realizar otra accion [y/n]" RESPUESTA
        if [ $RESPUESTA == y ]; then
		        clear
	                echo -e $IPTABLES
	elif [ $RESPUESTA == n ]; then
	exit 1
        fi	
	;;
res)
	iptables -P INPUT ACCEPT; iptables -P OUTPUT ACCEPT; iptables -P FORWARD ACCEPT; iptables -F; iptables -X 
	iptables -L -n --line-numbers && echo "Se restablecieron las politicas por defecto"	
	;;
back)
        iptables-save | tee -a $BACKUP && echo "El backup se realizo de manera exitosa"
	;;
move)
	iptables -L -n --line-numbers
	read -e -p "Ingrese numero de linea" NUMERO
	if [ $NUMERO -ge 1 ]; then
		iptables -D INPUT $NUMERO
	fi
	;;
       	
*)
	echo -e $IPTABLES
	exit 1
	;;
esac
