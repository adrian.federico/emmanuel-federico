#!/bin/bash

#Instituto: ISTEA
#Materia: Programacion 1
#Profesor: Sergio Pernas

#.......................#

#Tarea N°2 
#Ejercicio N°3
#Script N°1

echo "Ejecutando script N°1"
sleep 2s

#VARIABLE
LOGINDATE=$(date)


#EXPORTAR VARIABLE
su - -c "echo export LOGINDATE=\$\(date\) >> /etc/profile" root
export LOGINDATE=$(date)

sleep 2s
echo "Ejecutando script N°2"

#LLAMAR A SCRIPT N°2
echo "`/bin/sh ./script2.sh`"

