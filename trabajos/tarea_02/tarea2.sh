#!/bin/bash
#Tarea N°2
#Carrera: Soporte de Infraestruturas
#Materia: Programacion 1
#Profesor: Sergio Pernas


#PUNTO N°1

sleep 2s
echo "Iniciando copia de seguridad"
sleep 2s
clear
echo "Completando:%10"
sleep 1s
clear
echo "Completando:%60"
sleep 1s
clear
echo "Completado:%99"
sleep 1s
clear

#Crear directorio que contendra los archivos de log.
mkdir -p $HOME/registros

#Redirecciona a un fichero .log la hora de inicio del proceso de backup.
echo "Inicio del proceso: $(date).log" >> $HOME/registros/informe.log

#--------------------------------------------------------------#

###############
###VARIABLES###
###############

#Fecha de realizacion de Back-up.
DATE=$(date +%Y-%m-%d-%H%M%S)

#Carpeta que contendra la copia de seguridad.
CARPETA_DESTINO=$HOME/back-up

#Nombre de archivo
NOMBRE_ARCHIVO=backup-$USER

#Archivos a respaldar.
CARPETA_RESPALDAR=$HOME/documentos

#Directorio que contendra los registros de inicio y finalizacion del proceso de backup. 
LOG=/tmp/registros.log

#-------------------------------------------------------------------------------------#

##############
###COMANDOS###
##############

#Crear directorio que contendra el back-up.
mkdir -p back-up


#Archivos a respaldar.
echo "Archivos a respaldar"
sleep 2s
tar -cvf $CARPETA_DESTINO/$NOMBRE_ARCHIVO-$DATE.tar $CARPETA_RESPALDAR 



#Redircciona a un fichero .log la hora de finalizacion del proceso de backup.
echo "Fin del proceso: $(date).log" >> $HOME/registros/informe.log

sleep 2s 
echo "La copia de seguridad se realizo con exito"



#Punto N°2
#Login de usuarios.

mkdir -p $HOME/login_user

echo "Inicio de sesion:" $(who) >> $HOME/login_user/login.txt
