# Script realizado para segunda clase de Programacion 1.
# Instituto ISTEA.
# Carrera: Soporte de Infraestructura.
# Profesor: Sergio Pernas.

#VARIABLES
REG=/tmp/informe.txt

# Script para realizar Informe del sistema.

echo "Bienvenido a ISTEA" | tee -a $REG
sleep 1s
echo "Realizando informe" | tee -a $REG
sleep 2s
contador1='cargando 10%' 
contador2='cargando 50%' 
contador3='cargando 100%' 
echo $contador1
sleep 1s
echo $contador2
sleep 1s
echo $contador3
sleep 2s
echo "Version del Sistema Operativo:" | tee -a $REG
cat /etc/debian_version | tee -a $REG
sleep 1s
echo "Nombre de equipo: $HOSTNAME" | tee -a $REG
echo "El dispositivo tty: $(tty)" | tee -a $REG
sleep 1s
echo "Capacidad disco: Size Used Avail Use% Mount on" | tee -a $REG
df -h | grep /dev/sda1 | tee -a $REG
sleep 1s
echo "Direccion MAC:" | tee -a $REG
ip a | grep ether | tee -a $REG
sleep 1s
echo "Direccion IP:" | tee -a $REG
ip a | grep enp0s3 | tee -a $REG
sleep 1s
echo "El usuario es: $USER" | tee -a $REG
sleep 1s
echo "La ruta a su directorio es: $HOME" | tee -a $REG
sleep 1s
echo "La shell en uso es: $SHELL" | tee -a $REG
sleep 1s
echo "Realizando testeo de RED:"
sleep 2s
ping -c 3 www.google.com | tee -a $REG

sleep 2s
echo "Este informe se genero el: $(date)" | tee -a $REG
