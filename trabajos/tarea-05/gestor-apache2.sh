#!/bin/bash

source funciones.sh

#Tarea N°5

########################################################
##Carrera: Tecnicatura en Soporte de Infraestructuras.##
##Profesor: Sergio Pernas                             ##
##Alumno: Adrian Emmanuel Federico                    ##
########################################################


#Evaluar si soy usuario 'root'.

if [ ! $UID -eq 0 ];then
       echo "Ejecute este programa como usuario 'root'"
       exit 1
fi


#Variables
MENU="\t---------------------------------------------------------\n
\t|\t\t#INSTALACION STACK LAMP#\t\t|\n
\t|\t\tAPACHE - MariaDB - PHP\t\t\t|\n
\t|-------------------------------------------------------|\n
\t|\t1\tModificar direccion IP\t\t\t|\n
\t|\t2\tModificar Hostname\t\t\t|\n
\t|\t3\tInstalar Apache\t\t\t\t|\n
\t|\t4\tCrear Sitio\t\t\t\t|\n
\t|\t5\tAlta de Sitios\t\t\t\t|\n
\t|\t6\tBaja de Sitios\t\t\t\t|\n
\t|\t7\tEliminar archivos Sitios\t\t|\n
\t|\t8\tInstalar PHP\t\t\t\t|\n
\t|\t9\tInstalar MariaDB\t\t\t|\n
\t|\t10\tInstalar WORDPRESS\t\t\t|\n
\t|\t11\tBackup Sitios\t\t\t\t|\n
\t|\t12\tEstadisticas\t\t\t\t|\n
\t|\t0\tSalir\t\t\t\t\t|\n
\t--------------------------------------------------------"




while :;do

echo -e $MENU

read -e -p "Ingrese la opcion solicitada:" OPT

case $OPT in

1)      #Muestra en pantalla y realiza backup del arhivo de configuracion de la placa de Red 
	echo "La configuracion de Red actual:"
       	cat /etc/network/interfaces | tee -a config.bak
       
	#Definiendo direccion IP
	read -p "Presione ENTER para continuar"
        clear
	read -e -p "Ingrese la nueva direccion IP:" IP
	sed -i 's/address/address '$IP'/g' "plantilla-red.txt"
	
	#Definiendo mascara de subred
	read -e -p "Ingrese mascara de subred:" MASK
	sed -i 's/netmask/netmask '$MASK'/g' "plantilla-red.txt"

        #Definiendo default-gateway
	read -e -p "Ingrese default-gateway:" GW
	sed -i 's/gateway/gateway '$GW'/g' "plantilla-red.txt"

	#Copiar la plantilla con los nuevos datos ingresados
	cp plantilla-red.txt /etc/network/interfaces
	cat /etc/network/interfaces
	echo "Para aplicar los cambios debe reiniciar el sistema"
	read -e -p "Desea reiniciar ahora[y/n]:" RESPUESTA
	if [ $RESPUESTA == y ];then
		sudo reboot
		exit 1
	elif [ $RESPUESTA == n ];then
	        exit 1
	fi	
	;;
2)      #--------------------------------------------------------------------------#
	read -e -p "Ingrese nombre de Host:" NAME 
	mod_hostname $NAME
	
	echo "Para aplicar los cambios debe reiniciar el sistema"
	read -e -p "Desea reiniciar ahora[y/n]?:" RESPUESTA1
	if [ $RESPUESTA1 == y ];then
	      sudo reboot
	      exit 1
        elif [ $RESPUESTA1 == n ];then
  	exit 1
	fi
        clear
        ;;
3)      #--------------------------------------------------------------------------#
        read -e -p "Presione una tecla cualquiera para comenzar con la Instalacion"
        sudo apt update && apt install apache2 -y 
        sleep 1s
        echo "***Instalando Apache2***"
        echo "Cargando 10%"
	sleep 1s
	clear
        echo "Cargando 50%"
	sleep 1s
	clear
	echo "Cargando 100%"
	sleep 1s
        echo "Apache2 Instalado"	
        ;;
        #-------------------------------------------------------------------------#

4)      #Declarar 'vhost' utilizando una plantilla de configuracion.
        
        echo -e "Ingrese nombre de sitio de la siguiente manera\n
	\tEjemplo.com\n
	\t"
	read -e -p "Nombre de Sitio:" SITIO
	sed -i 's/ServerName/ServerName '$SITIO'/g' "plantilla-sitios.conf"
        echo -e "Cree nombre de direcorio para el Sitio:\n
        \tEjemplo.com\n
	\t"
	read -e -p "Nombre directorio:" DIR
	mkdir -p /var/www/$DIR
	echo "La ruta al sitio creado es:"
	cat /var/www/$DIR
	sed -i "s%DocumentRoot%DocumentRoot /var/www/$DIR%g" "plantilla-sitios.conf"
        echo -e "Ingrese nombre de fichero de Log para el Sitio:\n
	\tEjemplo.com.log\n
	\t"
	read -e -p "Ingrese nombre del fichero:" LOG
	#mkdir -p /var/log/apache-2
	touch /var/log/apache2/$LOG
	echo "La ruta al fichero de Log: /var/log/apache2/$LOG"
	sed -i "s%CustomLog%CustomLog /var/log/apache2/$LOG%g" "plantilla-sitios.conf"

	#Mover plantilla con los datos ingresados a Sites-available
	cp plantilla-sitios.conf /etc/apache2/sites-available/$SITIO.conf
	echo "El Sitio se ha creado con Exito"
	exit 1
	;;

        #-------------------------------------------------------------------------#

5)	#Activar Sitio
	echo "Sitios a activar:"
	ls  /etc/apache2/sites-available
	read -e -p "Ingrese el Sitio que desea activar:" ACT
        a2ensite $ACT
	systemctl reload apache2
	;;
	
        #-------------------------------------------------------------------------#
6)
        echo -e "Para eliminar un sitio ingrese:\n
	\tEjemplo.com.conf"
	echo "Sitios Enabled:"
        ls -l /etc/apache2/sites-enabled
	read -e -p "Ingrese el nombre del Sitio:" DEL
	a2dissite $DEL
	systemctl reload apache2
	;;
        #------------------------------------------------------------------------#	
7) 
        echo -e "Eliminar los arhivos de configuracion de Sitio\n
	\tEjemplo.com"
        read -e -p "Ingrese nombre de Sitio" SITIO
	rm -r /etc/apache2/sites-available/$SITIO.conf
	rm -r /var/www/$SITIO
	rm -r /var/log/apache-2/$SITIO.log
	echo "Los archivos de han eliminado con Exito"
	;;
	

       #------------------------------------------------------------------------#

8)
       read -e -p "Presione ENTER para comenzar con la Instalacion de PHP"
       apt install php libapache2-mod-php php-mysql
       systemctl restart apache2
       echo "PHP se ha instalado correctamente"
       ;;
       #-------------------------------------------------------------------------#

9)
       read -e -p "Presione ENTER para comencar con la Instalacion de MariaDB"
       apt install default-mysql-server
       systemctl restart apache2
       echo "Se ha instalado correctamente MariaDB"
       ;;
       #-------------------------------------------------------------------------#

10)     
       read -e -p "Presione ENTER para comenzar con la Instalacion de WORDPRESS"
       cd /var/www/html
       wget https://wordpress.org/latest.tar.gz
      
       #Descomprimir archivo
       echo "Descomprimiendo archivos..."
       sleep 1s
       clear
       echo "Descomprimiendo archivos....."
       sleep 1s
       clear
       echo "Descomprimiendo archivos......."
       sleep 1s
       clear
       echo "Descomprimiendo archivos.........." 
       sleep 1s
       clear
       echo "Descomprimiendo aechivos............."
       sleep 1s
       clear
       echo "Archivos descomprimidos"
       tar xf latest.tar.gz
       ls
       sleep 2s
        
       #Configuracion Wordpress
       echo "Configurando WORDPRESS"
       cd wordpress      
       echo "Copiando Ficheros de configuracion"
       cp wp-config-sample.php wp-config.php      
       sleep 1s
       
       #Configuracion de fichero wp-config.php 
       #Nombre
       sed -i 's/database_name_here/wordpress/g' "wp-config.php"
       #Nombre de usuario
       sed -i 's/username_here/user/g' "wp-config.php"
       #Password
       sed -i 's/password_here/1234/g' "wp-config.php"
       echo -e "Se ha configurado correctamente el fichero wp-config.php con las siguientes credenciales:\n
       \tDB NAME=Wordpress\n
       \tDB USER=User\n
       \tDB PASSWORD=1234\n
       \t"
       sleep 2s      

       #Configurar MariaDB 
       echo "Cracion de Base de datos"
       # mysql
       mysql  CREATE DATABASE wordpress;
                                    
       #Crear Usuario con privilegios
       echo "Configurando credenciales en DB"
       sleep 1s
       GRANT ALL ON wordpress.* TO 'user'@'localhost' IDENTIFIED BY '1234':

       #Flush de privilegios
       FLUSH PRIVILEGES;
       quit
       
       sleep 2s
       echo "Saliendo de DB"

       ;;

       #-------------------------------------------------------------------------#
       
11)
       echo -e "Realizar copia de seguridad de arvhivos de Sitio\n
       \tEjemplo.com\n
       \t"
       mkdir -p /var/backups/sitios
       read -e -p "Ingrese Sitio a respaldar:" SITIO
       cp -r /etc/apache2/sites-available/$SITIO.conf /var/backups/sitios/$SITIO.conf.bak
       cp -r /var/www/$SITIO /var/backups/sitios/$SITIO.bak
       cp -r /var/log/apache2/$SITIO.log /var/backups/sitios/$SITIO.log.bak 
       echo "Ralizando backup"
       sleep 2s
       echo "La ruta a los archivos Backapeados"
       ls -l /var/backups/sitios/$SITIO/conf.bak
       ls -l /var/backups/sitios/$SITIO.bak
       ls -l /var/backups/sitios/$SITIO.log.bak
       sleep 1s
       echo "Backup realizado"
       ;;
       
       #--------------------------------------------------------------------------#    

12)
       echo -e "Ingrese el nombre del sitio que desea ver las estadisticas:\n
       \tEjemplo.com\n
       \t"
       read -e -p "Ingrese nombre de Sitio:" SITIO
       #echo "Direccion IP de quien visita:"
       #cat /var/log/apache2/$SITIO.log | cut -d " "  -f 1 
       echo -e "N°\tIP CLIENTE\tRECURSO"
       cd /var/log/apache2/ ; nl $SITIO.log | cut -d " " -f 6,12,13,14
       echo "Total de Visitas:"
       cat /var/log/apache2/$SITIO.log | wc -l
       ;;      
       
       #--------------------------------------------------------------------------#
       
0)
	break
	;;

	#-------------------------------------------------------------------------#
*)    
       echo -e $MENU
       exit 1
       ;;
esac
done

