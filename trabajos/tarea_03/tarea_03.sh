#!/bin/bash
#CARRERA: Soporte de Infraestructura
#MATERIA: Programacion 1
#PROFESOR: Sergio Pernas
#ALUMNO: Adrian Emmanuel Federico

#TAREA N°4
#Generador de permisos

#PUNTO 1
sleep 1s 
echo "BIENVENIDO AL GESTOR DE PERMISOS DE DEBIAN 10"
sleep 1s
read -p "ANTES DE COMENZAR POR FAVOR INGRESE SU NOMBRE DE USUARIO:" USUARIO
read -p "INGRESE ARCHIVO:" ARCHIVO
test $ARCHIVO && echo "BUSCANDO ARCHIVO" || echo "NO HA SELECCIONADO UN ARCHIVO"
sleep 2s
echo "SELECCIONE PERMISOS:"
echo "0= Sin permisos"
echo "1= Ejecucion"
echo "2= Escritura"
echo "3= Escritura y ejecucion"
echo "4= Lectura"
echo "5= Lectura y ejecucion"
echo "6= Lectura y escritura"
echo "7= Lectura, escritura y ejecucion"
sleep 1s
read -p "INGRESE LOS PERMISOS DESEADOS, FORMATO NNN:" PERMISOS
test $PERMISOS && chmod $PERMISOS $ARCHIVO || echo "LOS PERMISOS DESEADOS NO SON CORRECTOS"  
sleep 1s
echo "LOS PERMISOS SE HAN APLICADO CORRECTAMENTE"
echo "Se han modificado los permisos del archivo: $ARCHIVO-$PERMISOS-$USUARIO-$(date)" >> registro.log     
sleep 2s

#PUNT0 2
echo "MODIFIQUE PROPIETARIO Y GRUPO DE FICHERO"
sleep 1s
read -p "FICHERO:" FICHERO
read -p "PROPIETARIO:" PROPIETARIO
read -p "GRUPO:" GRUPO
test $UID -eq 0 || echo "INGRESE PASSWORD DE ADMINISTRADOR"
su - -c "chown $PROPIETARIO:$GRUPO $HOME/$FICHERO" root
echo "Se han modificado Fichero-Grupo-Propietario: $FICHERO-$GRUPO-$PROPIETARIO-$(date)" >> registro.log
sleep 2s

#PUNTO 3
echo "DESEA GENERAR NUEVO GRUPO"
read -p "yes:" YES "NO" NO
test $YES && read -p "NOMBRE DE GRUPO:" GADD
su - -c "groupadd $GADD"
sleep 1s
echo "EL GRUPO SE HA GENERADO CON EXITO"
echo "Se ha creado el grupo:" $GADD >> registro.log
sleep 2s

#PUNTO 4
read -p "PRESIONE ENTER PARA LISTAR GRUPOS EXISTENTES"
echo "GRUPOS EXISTENTES:"
cut -d: -f1 /etc/group
sleep 2s

#PUNTO 5
echo "BUSCAR FICHEROS POR TIPO DE PERMISOS" 
read -p "INGRESE LOS TRES DIGITOS PARA LOS PERMISOS DESEADOS:" PERM   
sleep 1s 
test $PERM && find / -perm $PERM 
sleep 2s
echo ""
echo "---------------------------------"
echo ""
echo "RESUMEN DE LOS CAMBIOS REALIZADOS"
sleep 1s
cat registro.log
test -f
sleep 1s
echo "EL EXIT STATUS DE ESTE SCRIPT ES: $?"    

