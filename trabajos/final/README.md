![](https://campus.istea.edu.ar/pluginfile.php/1/theme_moove/logo/1613071850/LogoIstea.png)

### Proyecto Final Programacion 1

#### Profesor: Sergio Pernas
#### Alumno: Adrian Emmanuel Federico

 En el siguiente proyecto se intento aplicar todo los temas abarcados a lo largo del cuatrimestre, tratando de volcar en un proyecto integrador todos los conocimiento obtenido.
- Variables especiales 
- Hacer uso de arrays
- Hacer uso de variables de entorno.
- Validacion de entrada de datos
- Estructuras de control (if, case, while, for)
- Funciones
- Ayuda
- Instalacion de programas necesarios.


#### Link
https://gitlab.com/adrian.federico/emmanuel-federico/-/tree/main/trabajos/final

### Servidor DHCP
En este proyecto final consta de la instalacion y configuracion de un servidor DHCP. El cual sera el encargado de proporcionar entre otros parametros las direcciones IP a una red LAN.


#### Funciones  y caracteristicas

- Instalar servicio de DHCP
- Configurar placa de Red
- Seleccionar Interfaz de Red
- Establecer parametros servidor DHCP
- Inicializar servicio DHCP
- Estado servicio DHCP
- Reiniciar servicio DHCP
- DHCP leases
- Prueba de conectividad PC cliente
- Reestablecer configuracion de placa de Red
- Reserva de direcciones IP
- Backup de archivos de configuracion.



#### Ficheros

**Fichero Principal**

**/dhcp.sh**
Este sera el fichero principal del proyecto el cual utilizara funciones establecidas en otro script, Ademas de plantillas de configuracion establecidas en otros ficheros.


**Ficheros secundarios**

**/plantilla-red.conf**
Este fichero sera una plantilla para configurar los parametros de la placa de Red.
- Direccion IP
- Mascara de subred
- Default-gateway

Los cuales una vez establecidos estos parametros se copiara la plantilla al archivo de configuracion original /etc/network/interfaces.

**/plantilla-isc-dhcp.server.conf**
En este fichero se configurara la interfaz de Red utilizada para nuestro servidor DHCP.
Una vez completada la plantilla se copiara la configuracion a /etc/default/isc-dhcp-server.

**/plantilla-dhcpd.conf**
En este fichero se configuraran los parametros que utilizara nuestro servidor DHCP.
- Subnet
- Mascara de subred
- Scope
- Servidor DNS
- Default-gateway
- Reservacion de direcciones IP

La configuracion establecida en esta plantilla se copiara al fichero de configuracion /etc/dhcp/dhcpd.conf.


**/funciones.sh**
En este fichero se configurara la funcion que sera utilizada por el script principal **/dhcp.sh ** para realizar prueba de conectividad con el equipo cliente.


#### Variables

Se establecieron las siguientes variables:
**DATE**= Contendra la fecha y hora

**MENU**= El cual contendra el menu ciclico de opciones del programa el cual se utilizara mediante el comando **While**.



### Script
######  Paso 1
Para este script se a utilizado la estructura de control **case** la cual contendra las opciones del menu.

Antes de iniciar el script hacemos una pequeña vaidacion comprobando el UID del usuario para que si no es igual a 0, ejecute el script como usuario 'root'.
Luego con el comando **wich** buscamos si el servicio de DHCP esta instalado de no ser asi actualizara el sistema e instalara el paquete de DHCP.

######  Paso 2
Se realizara la configuracion de la placa de Red (direccion IP, mascara de subred, default-gateway) utilizando el comando **sed** con la opcion **-i** que tomara mediante los datos ingresados a la variable y los alojaran en la plantilla **/plantilla-red.conf** y luego copiada esa configuacion a /etc/network/interfaces.

######  Paso 3
En este punto se configurara cual sera la interfaz utilizada por el servidor DHCP para entragar direcciones.
Utilizando el comando **sed -i ** insertamos los datos obtenidos en la variable **$INT** en la plantilla **/plantilla-isc-dchp.server.conf** luego copiada a **/etc/default/isc-dhcp-server**.

######  Paso 4
Se configuraran los parametros utilizados por el servidor DHCP.
- Subnet
- Mascara de subred
- Servidor DNS
- Default-gateway
- Reserva direcciones IP

Tomando el valor de las varibales ingresadas insertandolos en **/etc/plantilla-dhcp.conf** y copiando a **/etc/dhcp/dhcpd.conf.**

######  Paso 5
Una vez realizadas las configuraciones se procede a iniciar el servicio de DHCP, utilizando **service isc-dhcp-server start.**

######  Paso 6
Vizualizar el estado del servicio de DHCP.
**service isc-dhcp-server status.**

######  Paso 7
Reiniciar el serivio de DHCP.
**service isc-dhcp-server restart.**

######  Paso 8
Listar leases para ver los clientes a los cuales el servidor les otorgo una direccion IP.
Hacemos un **cat** del fichero **/var/lib/dhcp/dhcpd.leases.**

######  Paso 9
Realizar prueba de conectividad con PC cliente mediante la funcion declara en **/funciones.sh** , la cual realizara un ping a la direccion IP otorgada a la PC cliente.

######  Paso 10
Reestablecer configuracion actual de placa de red.
Al iniciar el script se realizo una copia de la configuracion de la placa de red y se alojo en el archivo **config.Bak** el cual se volvera a copiar a la configuracion de la interfaz **/ect/network/interfaces.**

######  Paso 11
En este punto se establecen los parametros para agregar una reserva de direcciones IP al servidor y se congiguran en el arvhivo de configuracion** /etc/dhcp/dhcpd.conf**, previamente en este archivo de configuracion se edito con los paramentros:
- hostx
- Hardware ethernetx
- Fixed-addressx

Para ser remplazados por las variables de entorno ingresadas e insertadas mediante el comando **sed** al archivo de configuracion.


######  Paso 12
Se realiza el backup de los archivos de configuracion.
- /etc/default/isc-dech-server
- /etc/dhcp/dhcpd.conf

Se comprime estos arvchivos utilizando el comando** tar**  y se los envia a la ruta **/var/backups/dhcp.**


#### Recomendaciones
Para el correcto fncionamiento del servidor DHCP, la interfaz de red que se utilize debe estar en modo red interna.



##### End
