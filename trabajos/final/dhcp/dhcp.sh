#!/bin/bash


#FINAL PROGRAMACION 1

########################################
##Carrera: Soporte de Infraestructuras##
##Profesor: Sergio Pernas             ##
##Alumno: Adrian Emmanuel Federico    ##
########################################



#SERVIDOR DHCP
#--------------------------------------


#LLAMAR FUNCIONES
source funciones-final.sh


#VARIABLES

DATE=$(date +%Y-%m-%d-%H%M%S)

MENU="\t\n
\t-----------------------------------------------------------------\n
\t|\t\tINSTALACION DE SERVIDOR DHCP\t\t\t|\n
\t|---------------------------------------------------------------|\n
\t|\t1\tInstalar servicio DHCP\t\t\t\t|\n
\t|\t2\tConfigurar placa de Red\t\t\t\t|\n
\t|\t3\tSeleccionar Interfaz de Red\t\t\t|\n
\t|\t4\tEstablecer parametros servidor DHCP\t\t|\n
\t|\t5\tInicializar servicio DHCP\t\t\t|\n
\t|\t6\tEstado servicio DHCP\t\t\t\t|\n
\t|\t7\tReiniciar servicio DHCP\t\t\t\t|\n
\t|\t8\tDHCP leases\t\t\t\t\t|\n
\t|\t9\tPrueba de conectividad PC cliente\t\t|\n
\t|\t10\tReestablecer configuracion placa de Red\t\t|\n
\t|\t11\tReservar direcciones IP\t\t\t\t|\n
\t|\t12\tBackup archivos de configuracion\t\t|\n
\t|\t0\tSalir\t\t\t\t\t\t|\n
\t----------------------------------------------------------------\n
\t"



#----------------------------------------------------------------

#Evaluar si soy usuario 'root'.

if [ ! $UID -eq 0 ];then
	echo "Ejecute este Programa como usuario 'root'."
	exit 1
fi

#----------------------------------------------------------------
        
 
while :;do

echo -e $MENU

read -e -p "Ingrese opcion deseada:" OPC

case $OPC in

1)
	#Instalar servidor DHCP
        
	#Testear si el servicio de DHCP ya se encuentra instalado en el Servidor
	if ! which dhcpd > /dev/null 2>&1;then
		apt update && apt install isc-dhcp-server
	fi
	;;
        #------------------------------------------------------------------------#
2)
        #Listar interfaces conectadas
        echo "Configuracion de placa de RED"
	ip a
        
	echo "Configuracion de Red actual"
	cat /etc/network/interfaces | tee -a config.bak
	echo "Se ha realizado una copia de seguridad alojada en config.bak"
        read -e -p "Seleccione una Interfaz:" INT
	read -e -p "Ingrese direccion IP:" IP
        
	if [ $INT == enp0s3 ];then
	sed -i 's/address/address '$IP'/g' "plantilla-red.conf"
	read -e -p "Ingrese mascara de subred:" MASK
	sed -i 's/netmask/netmask '$MASK'/g' "plantilla-red.conf"
	read -e -p "Ingrese default-gateway:" GW
	sed -i 's/gateway/gateway '$GW'/g' "plantilla-red.conf"
        cp plantilla-red.conf /etc/network/interfaces
	service networking restart
        #echo "Para aplicar los cambios debe reiniciar el sistema"
       	

        elif [ $INT == enp0s8 ];then
	sed -i 's/address/address '$IP'/g' "plantilla-red-2.conf"
        read -e -p "Ingrese mascara de subred:" MASK
	sed -i 's/netmask/netmask '$MASK'/g' "plantilla-red-2.conf"
        read -e -p "Ingrese default-gateway:" GW
        sed -i 's/gateway/gateway '$GW'/g' "plantilla-red-2.conf"
        cp plantilla-red-2.conf /etc/network/interfaces
        service networking restart	
        #echo "Para aplicar los cambios debe reiniciar el sistema"
	fi
	

	echo "Para aplicar los cambios debe reiniciar el sistema"
	read -e -p "Desea reinicar ahora[y/n]:" RESPUESTA
	if [ $RESPUESTA == y ];then
	        sudo reboot
	        exit 1
	elif [ $RESPUESTA == n ];then
	        exit 1
	fi
	;;	

       #------------------------------------------------------------------------#

3)
       echo "Ingrese interfaz seleccionada en el punto anterior para configurar en el archivo de configuracion"
       read -e -p "Interfaz:"  INT
       sed -i 's/INTERFACESv4=""/INTERFACESv4="'$INT'"/g' "plantilla-isc-dhcp.server.conf"
       cp plantilla-isc-dhcp.server.conf /etc/default/isc-dhcp-server
       sleep 1s
       echo "Se ha configurado la interfaz $INT en el archivo de configuracion"
       ;;
       
       #------------------------------------------------------------------------#

4)    
      #Configurando Subnet
      echo -e "Ingrese Subnet:\n
      \tFORMATO= 10.XX.XX.XX\n
      \t" 
      read -e -p "Ingrese Subnet:" SUB
      sed -i 's/subnet x/subnet '$SUB'/g' "plantilla-dhcpd.conf"
      sleep 1s
 
      #Configurando mascara de subred
      read -e -p "Ingrese mascara de subred:" MASK
      sed -i 's/netmask x/netmask '$MASK'/g' "plantilla-dhcpd.conf"
      sleep 1s

      #Creando scope
      echo "Configuracion de Scope" 
      echo "Ejemplo: 10.0.0.1 10.0.0.255"
      read -e -p "Ingrese rango de direcciones IP:" SCP
      sed -i "s/range x/range $SCP/g" "plantilla-dhcpd.conf"
      sleep 1s

      #Determinar direccion IP de servidor DNS
      read -e -p "Ingrese direccion IP del servidor DNS:" DNS
      sed -i 's/dns x/option domain-name-servers '$DNS'/g' "plantilla-dhcpd.conf"
      sleep 1s

      #Determinar default-gateway
      read -e -p "Ingrese default gateway:" GW
      sed -i 's/option routers x/option routers '$GW'/g' "plantilla-dhcpd.conf"
      sleep 1s

      #Determinar direccion ip de Broadcast
      read -e -p "Ingrese direccion IP de Broadcast:" BRD
      sed -i 's/option broadcast-address x/option broadcast-address '$BRD'/g' "plantilla-dhcpd.conf"

      #Copiar plantilla 
      cp plantilla-dhcpd.conf /etc/dhcp/dhcpd.conf

      ;;
      #------------------------------------------------------------------------#

5)
      read -e -p "Presione ENTER para inicializar el servicio de DHCP"
      /etc/init.d/isc-dhcp-server start
      ;;
      
      

      #------------------------------------------------------------------------#     
6) 
      #Visualizar estado servicio DHCP

      read -e -p "Presione ENTER para vizualizar el estado del sevicio DHCP"
      service isc-dhcp-server status
      ;;       
      
      
      #------------------------------------------------------------------------#     
7)
      #Reiniciar servicio DHCP
      read -e -p "Desea reiniciar el servicio de DHCP ahora[y/n]:" RESPUESTA

      if  [ $RESPUESTA == y ] ;then
	      service isc-dhcp-server restart
	      exit 1
      elif [ $RESPUESTA == n ];then
	      exit 1
      fi
      ;;

      #------------------------------------------------------------------------#

8)
      read -e -p "Presione ENTER para ver el leases"
      cat /var/lib/dhcp/dhcpd.leases
      ;;

      #------------------------------------------------------------------------#

      #Realizar prueba de conectividad con Pc cliente mediante uan funcion.
9)    echo "Realizar prueba de conectividad"
      read -e -p "Ingrese direccion IP del cliente:" CLI
      test_ping $CLI
      ;; 

      #------------------------------------------------------------------------#
10)
      echo "Reestablecer configuracion de placa de Red"
      read -e -p "Desea reestablacer configuracion de placa de red[y/n]" RESPUESTA2
      if [ $RESPUESTA2 == y ];then
	      cp config.bak /etc/network/interfaces
	      service networking restart
      elif [ $RESPUESTA2 == n ];then
	      exit 1
      fi
      ;;

      #-------------------------------------------------------------------------#

11)
       echo "Reservacion de direcciones IP"
       read -e -p "Ingrese nombre de reservacion:" HOST
       sed -i 's/#hostx/host '$HOST'/g' "/etc/dhcp/dhcpd.conf"

       read -e -p "Ingrese MAC-ADDRESS:" MAC
       sed -i 's/#hardware ethernetx/hardware ethernet '$MAC'/g' "/etc/dhcp/dhcpd.conf"


       read -e -p "Ingrese la direccion IP deseada para la reservacion:" RESERVA
       sed -i 's/#fixed-addressx/fixed-address '$RESERVA'/g' "/etc/dhcp/dhcpd.conf"


       sed -i 's/#x}/}/g' "/etc/dhcp/dhcpd.conf" 

       #cp plantilla-dhcp.conf /etc/dhcp/dhcpd.conf
       service isc-dhcp-server restart
       ;;
      
       #---------------------------------------------------------------------#

12) 
       #Crear backup archivos de configuracion.
       echo "Realizar backup de archivos de configuracion de DHCP"
       sleep 1s
       
       #Crear directorio que contendra arvhivos de backup
       mkdir -p /var/backups/dhcp

       
       cp -r /etc/default/isc-dhcp-server /var/backups/dhcp/isc-dhcp.bak
       cp -r /etc/dhcp/dhcpd.conf /var/backups/dhcp/dhcpd.conf.bak
       
       #Comprimir archivos
       touch /var/log/dhcp-error.log
       tar -cvf /var/backups/dhcp/config-dhcp.$DATE.tar  /var/backups/dhcp >  /var/log/dhcp-error.log  2>&1
       ;;



    #------------------------------------------------------------------------#      

0)    
       break
       ;;

      #------------------------------------------------------------------------#

*)
        echo -e $MENU
	exit 1
	;;

esac
done
 




